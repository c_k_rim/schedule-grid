import * as moment from 'moment';
import {Resource} from './Resource';
import {RangeType, Slot} from './Slot';

export interface IQuote {
    dates?: string[];
    type: RangeType;
    start: string;
    end: string;
}

export interface ISchedule {
    resourceId: number;
    date: moment.Moment;
    step: number;
    work: {
        start: string;
        end: string;
    };
    quotas?: IQuote[];
}

export class Schedule {
    resourceId: number = null;
    resource: Resource = null;
    date: moment.Moment = null;
    work = {};
    slots: Slot[] = [];
    records = [];

    constructor(data: ISchedule) {
        this.resourceId = data.resourceId;
        this.date = moment(data.date);
        this.work = data.work;

        data.quotas.forEach(q => {
            if (!q.dates || q.dates.map(d => moment(d, 'DD.MM.YYYY')).some(d => d.isSame(this.date, 'day'))) {
                const startTime: moment.Moment = this.date.clone().add(+q.start.split(':')[0], 'hours').add(+q.start.split(':')[1], 'minutes'),
                    endTime: moment.Moment = this.date.clone().add(+q.end.split(':')[0], 'hours').add(+q.end.split(':')[1], 'minutes');

                if (q.type == RangeType.APPOINTMENT) {
                    for (const d: moment.Moment = startTime.clone(); d.isBefore(endTime); d.add(data.step, 'minutes')) {
                        const slot: Slot = new Slot();
                        slot.start = d.clone();
                        slot.end = d.clone().add(data.step, 'minutes');
                        slot.type = q.type;

                        this.slots.push(slot);
                    }
                } else {
                    this.slots = this.slots.filter(s => s.type != RangeType.APPOINTMENT || (s.type == RangeType.APPOINTMENT && !(s.start.isBetween(startTime, endTime) || s.end.isBetween(startTime, endTime))));

                    const slot: Slot = new Slot();
                    slot.start = startTime;
                    slot.end = endTime;
                    slot.type = q.type;

                    this.slots.push(slot);
                }

            }
        });
        this.slots = this.slots.sort((sA, sB) => sA.start.isBefore(sB.start) ? -1 : 0);
    }
}
