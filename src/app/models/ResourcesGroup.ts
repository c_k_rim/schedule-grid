import {Resource} from './Resource';

export class ResourcesGroup {
    name: string;
    items: Resource[];
}
