import * as moment from 'moment';

export interface IResource {
    id: number;
    name: string;
    spec: string;
    mi: string; // Medical institution (Медицинское учреждение )
    office: string;
}

export class Resource {
    id: number;
    name: string;
    spec: string;
    mi: string; // Medical institution (Медицинское учреждение )
    office: string; // Кабинет

    constructor(data: IResource) {
        this.id = data.id;
        this.name = data.name;
        this.spec = data.spec;
        this.mi = data.mi;
        this.office = data.office;
    }

    worktime: {
        duration: moment.Duration;

        weekday: {
            num: number;
            duration: string[];
        }[];
    };

    schedule: {
        step: number; // Schedule grid step (Шаг сетки расписания) IN MINUTES (в минутах)
        ranges: {

            type: string;
            name: string;

            weekday: {
                num: number;
                duration: string[];
            }[];
        }[]; // Range with quota (Диапазоны с квотой)
    }; // Данные расписания
}
