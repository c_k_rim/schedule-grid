import * as moment from 'moment';
import {Patient} from './Patient';
import {Appointment} from './Appointment';
import {Resource} from './Resource';

export enum RangeType {
    APPOINTMENT, NOT_WORK, TRAININ, DOCUMENTS_WORK, VACATION, HOME_RECEPTION, SICK
}

export interface ISlotEvent {
    slot: Slot;
    x: number;
    y: number;
}

export class Slot {
    start: moment.Moment = null;
    end: moment.Moment = null;
    type: RangeType = null;
    patient: Patient = null;
    resource: Resource = null;
    appointment: Appointment = null;

    constructor () {}

    public fromAppointment(data: Appointment): void {
        this.appointment = data;
        this.type = RangeType.APPOINTMENT;
        this.start = data.date;
        this.patient = data.patient;
    }
}
