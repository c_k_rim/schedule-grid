import * as moment from 'moment';
import {Patient} from './Patient';

export interface IAppointment {
    id: number;
    patientId: number;
    resourceId: number;
    date: string;
}

export class Appointment {
    id: number;
    patient: Patient;
    patientId: number;
    resourceId: number;
    date: moment.Moment;

    constructor(data?: IAppointment) {
        if (data) {
            this.id = data.id;
            this.patientId = data.patientId;
            this.resourceId = data.resourceId;
            this.date = moment(data.date, 'DD.MM.YYYY HH:mm');
        }
    }

    public toIAppointment(): IAppointment {
        return {
            id: this.id,
            patientId: this.patientId,
            resourceId: this.resourceId,
            date: this.date.format('DD.MM.YYYY HH:mm')
        };
    }
}
