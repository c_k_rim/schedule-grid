import * as moment from 'moment';

export interface IPatient {
    id: number;
    name: string;
    birthdate: string;
    polis: string;
}

export class Patient {

    id: number;
    name: string;
    polis: string; // The number of medical insurance (Номер полиса ОМС)

    private _birthdate_format = 'DD.MM.YYYY';
    private _birthdate: moment.Moment;

    get birthdate(): string {
        return this._birthdate.format(this._birthdate_format);
    }

    set birthdate(value: string) {
        this._birthdate = moment(value, this._birthdate_format);
    }

    constructor(data: IPatient) {
        this.id = data.id;
        this.name = data.name;
        this.birthdate = data.birthdate;
        this.polis = data.polis;
    }
}
