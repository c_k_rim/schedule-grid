import {Resource} from './Resource';
import {ResourcesGroup} from './ResourcesGroup';

export class Resources {

    sortScenario: string;

    sortScenarios: { [key: string]: { name: string, func: () => void } } = {
        spec: {
            name: 'По специальностям',
            func: (): void => {

                const result: ResourcesGroup[] = [];

                this.groups.forEach(group => {
                    group.items.forEach(item => {
                        if (!result[item.spec]) {
                            result[item.spec] = {
                                name: item.spec,
                                items: []
                            };
                        }

                        result[item.spec].items.push(item);
                    });
                });

                result.map(group => {
                    group.items.sort((a, b) => a.name > b.name ? 1 : -1);
                    return group;
                });

                this.groups = result;

            }
        },

        name: {
            name: 'По алфавиту',
            func: (): void => {

                const result: ResourcesGroup[] = [{name: '', items: []}];

                this.groups.forEach(group => {
                    result[0].items = result[0].items.concat(group.items);
                });

                result[0].items.sort((a, b) => a.name > b.name ? 1 : -1);

                this.groups = result;
            }
        }
    };

    groups: ResourcesGroup[];

    constructor(resources: Resource[]) {
        this.groups = [{name: '', items: resources}];


        this.sort('name');
    }

    sort(scenario?: string): void {

        if (scenario) {

            if (scenario === this.sortScenario) {
                return;
            }

            this.sortScenario = scenario;
        }

        this.sortScenarios[this.sortScenario].func();
    }

    // search(text: string): void {
    //
    //     this.groups.forEach(group => {
    //         group.items.forEach(item => {
    //             item.active = item.name.indexOf(text) > -1 || item.spec.indexOf(text) > -1;
    //         });
    //     });
    //
    // }
}
