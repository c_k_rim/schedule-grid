import {Injectable} from '@angular/core';
import {IPatient, Patient} from '../models/Patient';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

@Injectable()
export class PatientService {
    private _patients: IPatient[] = [
        {id: 1, name: 'Иванов Иван Иванович', birthdate: '11.11.2011', polis: '1111111111111111'},
        {id: 2, name: 'Алексеев Алексей Алексеевич', birthdate: '22.12.1922', polis: '2222222222222222'},
        {id: 3, name: 'Петров Петр Петрович', birthdate: '01.01.1990', polis: '3333333333333333'},
        {id: 4, name: 'Сергеев Сергей Сергеевич', birthdate: '02.02.2002', polis: '4444444444444444'},
        {id: 5, name: 'Васильев Василий Васильевич', birthdate: '09.09.1949', polis: '5555555555555555'}
    ];

    private patient: Patient = null;

    constructor(private http: HttpClient) {
    }

    public getPatient(): Patient {
        return this.patient;
    }

    public searchPatient(txt: string, type: number): Observable<Patient[]> {
        const patients = type === 1 ? this._patients.filter(p => p.polis === txt) : this._patients.filter(p => p.name.indexOf(txt) != -1);
        return of(patients.map(p => new Patient(p)));
        // return this.http.get<IPatient>('api/patients')
        // .take(1)
        // .map(p => new Patient(p))
        // .find(p => type == 0 ? p.polis == txt : p.name.indexOf(txt) != -1)
        // .catch(error => {console.error('searchPatient', error); return error; });
    }

    public identPatient(patientId: number): Observable<Patient> {
        return of(this._patients.find(p => p.id == patientId));
    }
}
