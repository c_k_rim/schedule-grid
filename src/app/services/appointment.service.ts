import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {Appointment} from '../models/Appointment';
import {max, map} from 'rxjs/operators';

@Injectable()
export class AppointmentService {
    private _records = [
        { id: 1, patientId: 1, resourceId: 1, date: '04.04.2019 10:00' },
        { id: 2, patientId: 2, resourceId: 1, date: '04.04.2019 10:00' },
        { id: 3, patientId: 3, resourceId: 1, date: '04.04.2019 10:30' },
        { id: 4, patientId: 4, resourceId: 2, date: '02.04.2019 11:00' }
    ];

    constructor () {}

    public getRecordsByResource(resourceIds: number[]): Observable<Appointment[]> {
        return of(this._records
            .filter(r => resourceIds.some(rs => rs == r.resourceId))
            .map(r => new Appointment(r))
        );
    }

    public addRecord(data: Appointment): Observable<Appointment[]> {
        return of(this._records.map(r => r.id))
            .pipe(
                max(),
                map((lastId: number) => {
                    data.id = lastId;
                    this._records.push(data.toIAppointment());
                    return this._records.map(r => new Appointment(r))
                })
            );
    }

    public removeRecord(recordId: number): Observable<Appointment[]> {
        this._records = this._records.filter(r => r.id != recordId);
        return of(this._records.map(r => new Appointment(r)));
    }
}
