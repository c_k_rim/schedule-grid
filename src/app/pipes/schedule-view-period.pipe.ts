import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

import {Schedule} from '../models/Schedule';

@Pipe({name: 'schedulePeriod'})
export class ScheduleViewPeriodPipe implements PipeTransform {
    transform(value: Schedule[], dt: moment.Moment, qnt: number): Schedule[] {
        return value.filter(s => s.date.isSameOrAfter(dt, 'date') && s.date.isSameOrBefore(dt.clone().add(qnt - 1, 'days')));
    }
}
