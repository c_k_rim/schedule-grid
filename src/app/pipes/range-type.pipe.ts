import {Pipe, PipeTransform} from '@angular/core';
import {RangeType} from '../models/Slot';

@Pipe({name: 'rangeType'})
export class RangeTypePipe implements PipeTransform {
    transform(value: RangeType): string {
        switch (value) {
            case RangeType.APPOINTMENT:
                return 'Запись на прием';
            case RangeType.DOCUMENTS_WORK:
                return 'Работа с документами';
            case RangeType.HOME_RECEPTION:
                return 'Прием на дому';
            case RangeType.NOT_WORK:
                return 'Врач не работает';
            case RangeType.SICK:
                return 'Больничный';
            case RangeType.TRAININ:
                return 'Обучение';
            case RangeType.VACATION:
                return 'Отпуск';
            default:
                return '';
        }
    }
}


@Pipe({name: 'rangeClass'})
export class RangeClassPipe implements PipeTransform {
    transform(value: RangeType): string {
        switch (value) {
            case RangeType.APPOINTMENT:
                return 'appointment';
            case RangeType.DOCUMENTS_WORK:
                return 'documents-work';
            case RangeType.HOME_RECEPTION:
                return 'home-reception';
            case RangeType.NOT_WORK:
                return 'not-work';
            case RangeType.SICK:
                return 'sick';
            case RangeType.TRAININ:
                return 'trainin';
            case RangeType.VACATION:
                return 'vacation';
            default:
                return '';
        }
    }
}
