import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { FormsModule } from '@angular/forms';

import { SchedulePageComponent } from './pages/schedule/schedule.component';
import { PatientInfoComponent } from './components/patient-info/patient-info.component';
import { SlotComponent } from './components/slot/slot.component';
import { DoctorListComponent } from './components/doctor-list/doctor-list.component';
import { DaySlotsComponent } from './components/day-slots/day-slots.component';
import {PatientService} from './services/patient.service';
import {DatePickerComponent} from './components/date-picker/date-picker.component';
import {ResourcesService} from './services/resources.service';
import {KeysPipe} from './pipes/keys.pipe';

import * as moment from 'moment';
import {RangeClassPipe, RangeTypePipe} from './pipes/range-type.pipe';
import {ScheduleViewPeriodPipe} from './pipes/schedule-view-period.pipe';
import {RecordComponent} from './components/record-popup/record.component';
import {AppointmentService} from './services/appointment.service';

const routes: Routes = [
	{ path: '', component: SchedulePageComponent, pathMatch: 'full' }
];

const components = [
	PatientInfoComponent,
	DoctorListComponent,
	DaySlotsComponent,
	SlotComponent,
	DatePickerComponent,
	RecordComponent,
	KeysPipe,
	RangeTypePipe,
	RangeClassPipe,
	ScheduleViewPeriodPipe
];

@NgModule({
	declarations: [
		SchedulePageComponent,
		...components
	],
	imports: [RouterModule.forRoot(routes), CommonModule, FormsModule ],
	providers: [PatientService, ResourcesService, AppointmentService],
	exports: [RouterModule, CommonModule, ...components]
})
export class AppRoutingModule {
    constructor(){
        moment.locale('ru');
    }
}
