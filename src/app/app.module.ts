import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './api/in-memory-data.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule,
      HttpClientModule,
      InMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
