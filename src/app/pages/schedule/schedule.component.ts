import {AfterViewInit, ChangeDetectorRef, Component} from '@angular/core';
import * as moment from 'moment';

import {PatientService} from '../../services/patient.service';
import {ResourcesService} from '../../services/resources.service';
import {Resource} from '../../models/Resource';
import {ISlotEvent, Slot} from '../../models/Slot';
import {AppointmentService} from '../../services/appointment.service';
import {Appointment} from '../../models/Appointment';
import {Patient} from '../../models/Patient';

@Component({
    selector: 'app-schedule-page',
    templateUrl: './schedule.component.html',
    styleUrls: ['./schedule.component.scss']
})

export class SchedulePageComponent implements AfterViewInit {
    private selectedPatient: Patient;
    public selectedDoctors: number[] = [];
    public selectedDate: moment.Moment = moment();
    public selectedSlot: ISlotEvent = null;

    public resources: Resource[] = [];
    public records: {[key:number]: Appointment[]} = {};

    public selectPeriod: number = 1;
    public schedules = [];
    public isViewRecord: boolean = false;
    public positionPopup = { top: 0, left: 0 };

    constructor(private PatientService: PatientService,
                private ResourceService: ResourcesService,
                private AppointmentService: AppointmentService,
                private cdr: ChangeDetectorRef
    ) {
        this.ResourceService.getResources()
            .subscribe(r => this.resources = r);
    }

    ngAfterViewInit(): void {
        this.cdr.detectChanges();
    }

    public isDisabledCalendar(): boolean {
        return this.selectedDoctors.length === 0;
    }

    public onSelectDoctor(ids: number[]): void {
        this.selectedDoctors = ids;
        this.showSchedule();
    }
    public onSelectDate(date: moment.Moment): void {
        this.selectedDate = date;
        this.showSchedule();
    }

    public onSelectSlot(slot: ISlotEvent): void {
		this.selectedSlot = slot;
		this.positionPopup = { top: slot.y, left: slot.x };
		this.isViewRecord = true;
	}

    private showSchedule() {
        if (this.selectedDoctors.length > 0) {
            this.AppointmentService.getRecordsByResource(this.selectedDoctors)
                .subscribe(records => {
                    this.records = {};
                    records.forEach(r => {
                        if (this.records[r.resourceId]) { this.records[r.resourceId].push(r); }
                        else { this.records[r.resourceId] = [r]; }
                    });
                });
            this.ResourceService.getSchedules(this.selectedDoctors, this.selectedDate)
                .subscribe(s => {
                    this.schedules = s.sort((dA, dB) => dA.date.isBefore(dB.date) ? -1 : 0);
                });
        } else {
            this.schedules = [];
            this.records = {};
        }
    }

    public getResourceRecordsDate(rId: number, d: moment.Moment): Appointment[] {
        // console.info('getResourceRecordsDate',this.records[rId],rId,d,this.records[rId].filter(a => a.date.isSame(d, 'day')));
        return this.records[rId] ? this.records[rId].filter(a => a.date.isSame(d, 'day')) : [];
    }

    public onCancelRecord(): void {
        this.AppointmentService.removeRecord(this.selectedSlot.slot.appointment.id)
            .subscribe(appointments => {
                this.records = {};
                appointments.forEach(r => {
                        if (this.records[r.resourceId]) { this.records[r.resourceId].push(r); }
                        else { this.records[r.resourceId] = [r]; }
                    });
                this.isViewRecord = false;
            });
    }

    public onCreateRecord(slot: Slot): void {
        const r: Appointment = new Appointment();
        r.date = slot.start;
        r.patient = this.selectedPatient;
        r.patientId = this.selectedPatient.id;
        r.resourceId = slot.resource.id;

        this.AppointmentService.addRecord(r)
            .subscribe(() => {
                this.AppointmentService.getRecordsByResource(this.selectedDoctors)
                .subscribe(records => {
                    this.records = {};
                    records.forEach(r => {
                        if (this.records[r.resourceId]) { this.records[r.resourceId].push(r); }
                        else { this.records[r.resourceId] = [r]; }
                    });
                    this.isViewRecord = false;
                });
            });
    }

    public onSelectPatient(patient: Patient): void {
        this.selectedPatient = patient;
    }
}
