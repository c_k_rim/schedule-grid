import * as moment from 'moment';

const resources = [
    {
        id: 1,
        name: 'Григорьева Г.Г.',
        spec: 'Терапевт',
        mu: 'ГП №128',
        room: 'к.110',
        step: 30,
        workTime: [
            {day: 1, start: '10:00', end: '20:00'},
            {day: 2, start: '10:00', end: '20:00'},
            {day: 3, start: '10:00', end: '20:00'},
            {day: 4, start: '10:00', end: '20:00'},
            {day: 5, start: '10:00', end: '20:00'}
        ],
        period: {qnt: 2, duration: 'month'},
        quotas: [
            { day: 1, start: '10:00', end: '14:00', type: 'appointment'},
            { day: 1, start: '14:00', end: '15:00', type: 'not_work'},
            { day: 1, start: '15:00', end: '20:00', type: 'appointment'},
            { day: 2, start: '10:00', end: '14:00', type: 'appointment'},
            { day: 2, start: '14:00', end: '15:00', type: 'not_work'},
            { day: 2, start: '15:00', end: '20:00', type: 'appointment'},
            { day: 3, start: '10:00', end: '14:00', type: 'appointment'},
            { day: 3, start: '14:00', end: '15:00', type: 'not_work'},
            { day: 3, start: '15:00', end: '20:00', type: 'appointment'},
            { day: 4, start: '10:00', end: '14:00', type: 'appointment'},
            { day: 4, start: '14:00', end: '15:00', type: 'not_work'},
            { day: 4, start: '15:00', end: '20:00', type: 'appointment'},
            { day: 5, start: '10:00', end: '14:00', type: 'appointment'},
            { day: 5, start: '14:00', end: '15:00', type: 'not_work'},
            { day: 5, start: '15:00', end: '20:00', type: 'appointment'}
        ]
    },
    {
        id: 2,
        name: 'Сидорова С.С.',
        spec: 'Терапевт',
        mu: 'ГП №128',
        room: 'к.120',
        step: 30,
        workTime: [
            {day: 1, start: '08:00', end: '15:00'},
            {day: 2, start: '08:00', end: '15:00'},
            {day: 3, start: '08:00', end: '15:00'},
            {day: 4, start: '08:00', end: '15:00'}
        ],
        period: {qnt: 2, duration: 'month'},
        quotas: [
            { day: 1, start: '10:00', end: '15:00', type: 'appointment'},
            { day: 1, start: '10:00', end: '15:00', type: 'training'},
            { day: 2, start: '10:00', end: '15:00', type: 'appointment'},
            { day: 3, start: '10:00', end: '15:00', type: 'appointment'},
            { day: 4, start: '10:00', end: '15:00', type: 'appointment'},
        ]
    },
    {
        id: 3,
        name: 'Сидорова С.С',
        spec: 'Терапевт',
        mu: 'ГП №128',
        room: 'к.130',
        step: 10,
        workTime: [
            {day: 5, start: '14:00', end: '18:00'},
            {day: 6, start: '14:00', end: '18:00'}
        ],
        period: {qnt: 1, duration: 'month'},
        quotas: [
            { day: 5, start: '14:00', end: '18:00', type: 'appointment'},
            { day: 6, start: '14:00', end: '18:00', type: 'appointment'}
        ]
    },
    {
        id: 4,
        name: 'Елисеева Е.Е.',
        spec: 'Офтальмолог',
        mu: 'ГП №128',
        room: 'к.140',
        step: 30,
        workTime: [
            {day: 1, start: '08:00', end: '18:00'},
            {day: 2, start: '08:00', end: '18:00'},
            {day: 3, start: '08:00', end: '18:00'},
            {day: 4, start: '08:00', end: '18:00'},
            {day: 5, start: '08:00', end: '18:00'}
        ],
        period: {qnt: 2, duration: 'month'},
        quotas: [
            { day: 5, start: '14:00', end: '18:00', type: 'appointment'},
            { day: 6, start: '14:00', end: '18:00', type: 'appointment'}
        ]
    },
    {
        id: 5,
        name: 'Константинова-Щедрина А.А.',
        spec: 'Офтальмолог',
        mu: 'ГП №128',
        room: 'к.150',
        step: 30
    }
]
