import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {Patient} from '../../models/Patient';
import {Slot} from '../../models/Slot';

@Component({
    selector: 'record-popup',
    templateUrl: './record.component.html',
    styleUrls: ['./record.component.scss']
})

export class RecordComponent implements OnInit {
    @Input() slot: Slot = null;

    @Output('edit') editRecord: EventEmitter<Slot> = new EventEmitter();
    @Output('cancel') cancelRecord: EventEmitter<null> = new EventEmitter();
    @Output('view') viewRecord: EventEmitter<null> = new EventEmitter();


    public patient: Patient = null;

    constructor () {}

    ngOnInit(): void {
        this.patient = this.slot.patient;
    }

    public onViewRecord(): void {
        this.viewRecord.emit();
    }

    public onEditRecord(): void {
        this.editRecord.emit(this.slot);
    }

    public onCancelRecord(): void {
        this.cancelRecord.emit();
    }
}
