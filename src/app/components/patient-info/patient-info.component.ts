import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Patient} from '../../models/Patient';
import {PatientService} from '../../services/patient.service';

@Component({
    selector: 'patient-info',
    templateUrl: './patient-info.component.html',
    styleUrls: ['./patient-info.component.scss']
})

export class PatientInfoComponent {
    public patient: Patient = null;
    public searchResult: Patient[] = [];

    @Output('select') selectPatient: EventEmitter<Patient> = new EventEmitter();

    public isMenuOpen: boolean = false;
    public isNotFound: boolean = false;

    // realPatient: Patient[] = mockPatient;
    //
    // patient: Patient[];
    //
    // oldSearch: string;

    constructor(private PatientService: PatientService) {
        // this.patient = this.realPatient;
    }

    public onKeyUpSearch(txt: string): void {
        console.info('onKeyUpSearch', txt);
        this.isNotFound = false;
        if (txt && txt.length >= 3) {
            this.PatientService.searchPatient(txt, +(/[0-9]/.test(txt[0])))
                .subscribe(patients => {
                    if (patients && patients.length > 0) {
                        this.searchResult = patients;
                    } else {
                        this.isNotFound = true;
                    }
                });
        }
    }

    public onSelectPatient(patient: Patient): void {
        this.patient = patient;
        this.searchResult = [];
        this.selectPatient.emit(patient);
    }

    public onClosePatient(): void {
        // this.closePatient.emit();
        this.patient = null;
        this.isMenuOpen = false;
        this.selectPatient.emit(null);
    }

    //
    // OnSearchKeyup(event: any): void {
    //
    //     const search: string = event.target.value;
    //
    //     if (search !== this.oldSearch) { // Строка поиска изменилась
    //         this.patient = !search // ЕСЛИ Пустая строка поиска
    //             ? this.realPatient // Возвращаем нефильтрованный массив
    //             : this.realPatient.filter(i => i.name.indexOf(search) > -1);
    //
    //         this.oldSearch = search;
    //     }
    // }
}
