import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Patient} from '../../models/Patient';
import {ISlotEvent, RangeType, Slot} from '../../models/Slot';
import {Resource} from '../../models/Resource';

@Component({
    selector: 'slot',
    templateUrl: './slot.component.html',
    styleUrls: ['./slot.component.scss']
})

export class SlotComponent implements  OnInit {
	@Input() slot: Slot;
	@Input() resource: Resource;
	@Output('select') selectSlot: EventEmitter<ISlotEvent> = new EventEmitter();

	public isShowTime: boolean = false;

	constructor() { }

	ngOnInit(): void {
		this.isShowTime = this.slot.type == RangeType.APPOINTMENT || this.slot.patient != null;
	}

	public onSelectSlot(event: MouseEvent): void {
		this.slot.resource = this.resource;
		if (this.slot.type == RangeType.APPOINTMENT || this.slot.patient) {
			this.selectSlot.emit({slot: this.slot, x: event.clientX, y: event.clientY});
		}
	}
}
