import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import * as moment from 'moment';
import {Resource} from '../../models/Resource';
import {ISlotEvent, Slot} from '../../models/Slot';
import {Appointment} from '../../models/Appointment';
import {PatientService} from '../../services/patient.service';

@Component({
	selector: 'day-slots',
	templateUrl: './day-slots.component.html',
	styleUrls: ['./day-slots.component.scss']
})

export class DaySlotsComponent implements OnInit {
	private _day: moment.Moment = null;
	private _appointments: Appointment[] = [];
	public resultSlots: Slot[] = [];

	@Input()
	set day(value: moment.Moment) { this._day = value; }
	get day(): moment.Moment { return this._day; }
	@Input() resource: Resource = null;
	@Input()
    set appointments(value: Appointment[]) { this._appointments = value; this.reloadSlots(); }
	@Input() work: {start: string, end: string} = null;
	@Input() slots: Slot[] = [];

	@Output('slot-selected') selectedSlot: EventEmitter<ISlotEvent> = new EventEmitter();

	constructor (private patientService: PatientService) { }

    ngOnInit(): void { this.reloadSlots(); }

    private reloadSlots(): void {
		this.resultSlots = this.slots.filter(s => !this._appointments.some(r => r.date.isBetween(s.start, s.end, null, '[)')));
		this._appointments.forEach(a => {
			this.patientService.identPatient(a.patientId).subscribe(p => {
				a.patient = p;
				const slot: Slot = new Slot();
				slot.fromAppointment(a);
				this.resultSlots.push(slot);
				this.resultSlots = this.resultSlots.sort((dA, dB) => dA.start.isBefore(dB.start) ? -1 : 0);
			});
		});
	}

    public onSelectSlot(slot: ISlotEvent): void {
		this.selectedSlot.emit(slot);
	}

}
