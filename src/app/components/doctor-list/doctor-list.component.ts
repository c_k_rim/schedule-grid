import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Resource} from '../../models/Resource';

@Component({
    selector: 'doctor-list',
    templateUrl: './doctor-list.component.html',
    styleUrls: ['./doctor-list.component.scss']
})

export class DoctorListComponent {
    private _doctors: Resource[] = [];
    public _selected: number[] = [];
    public groupType = '';
    public groupedResource: { [key: string]: Resource[] } = {};

    public isFilterOpen: boolean = false;


    @Input('doctors')
    set doctors(value: Resource[]) {
        this._doctors = value;
        this.onChangeGroupType(this.groupType);
    }

    get doctors(): Resource[] {
        return this._doctors;
    }

    @Output('select') select: EventEmitter<number[]> = new EventEmitter();

    constructor() {
        this.onChangeGroupType('S');
    }

    public onChangeGroupType(type: string): void {
        this.groupType = type;
        if (type === 'S') {
            this.groupedResource = {};

            this._doctors.forEach(v => {
                const key: string = v.spec;
                if (!this.groupedResource[key]) {
                    this.groupedResource[key] = [v];
                }
                else {
                    this.groupedResource[key].push(v);
                }
            });
        } else if (type === 'L') {
            this.groupedResource = {};
            this._doctors.forEach(v => {
                const key: string = v.name[0].toUpperCase();
                if (!this.groupedResource[key]) {
                    this.groupedResource[key] = [v];
                } else {
                    this.groupedResource[key].push(v);
                }
            });
        }
    }

    public isSelected(id: number): boolean {
        return this._selected.indexOf(id) !== -1;
    }

    public onSelectDoctor(id: number): void {
        if (this._selected.indexOf(id) === -1) {
            this._selected.push(id);
        } else {
            this._selected = this._selected.filter(d => d !== id);
        }
        this.select.emit(this._selected);
    }

    public allSelected(isAll: boolean): void {
        if (isAll) {
            this._selected = this._doctors.map(d => d.id);
        } else {
            this._selected = [];
        }
        this.select.emit(this._selected);
        this.isFilterOpen = false;
    }
}
