import {Component, Input, Output, EventEmitter} from '@angular/core';
import * as moment from 'moment';

@Component({
    selector: 'date-picker',
    templateUrl: './date-picker.component.html',
    styleUrls: ['./date-picker.component.scss']
})

export class DatePickerComponent {
    @Input('is-disabled') isDisabled = true;

    @Output('on-select') selectDate: EventEmitter<moment.Moment> = new EventEmitter();

    public isOpen = false;
    public currentMonth: moment.Moment = moment();
    public monthDates: moment.Moment[] = [];
    public selectedDate: moment.Moment = moment();


    constructor() {
        this.calcMonth();
    }

    private calcMonth(): void {
        const startDate: moment.Moment = this.currentMonth.clone().startOf('month').startOf('week'),
            endDate: moment.Moment = this.currentMonth.clone().endOf('month').endOf('week');

        this.monthDates = [];
        for (const d = startDate.clone(); d.isSameOrBefore(endDate, 'day'); d.add(1, 'day')) {
            this.monthDates.push(d.clone());
        }
    }

    public prevMonth(): void {
        this.currentMonth.add(-1, 'month');
        this.calcMonth();
    }

    public nextMonth(): void {
        this.currentMonth.add(1, 'month');
        this.calcMonth();
    }

    public isToday(d: moment.Moment): boolean {
        return moment().isSame(d, 'day');
    }

    public isInactive(d: moment.Moment): boolean {
        return d.isBefore(moment(), 'day') || !d.isSame(this.currentMonth, 'month');
    }

    public onSelectDate(date: moment.Moment): void {
        this.selectedDate = date;
        this.selectDate.emit(date);
        this.isOpen = false;
    }
}
